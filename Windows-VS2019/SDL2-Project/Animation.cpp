#include "Animation.h"

#include "SDL2Common.h"

//for printf
#include <stdio.h>

/**
 * initAnimation
 * 
 * Function to populate an animation structure from given paramters. 
 * 
 * @param anim Animation structure to populate 
 * @param noOfFrames Frames of animation
 * @param SPRITE_WIDTH Width of the sprite
 * @param SPRITE_HEIGHT Height of the sprite
 * @param row Row of the grid to take the sprites from or -1 if working along a row. 
 * @param col Column of the grid to take the sprites from or -1 if along a column.
 * @return void?  Could do with returning a value for success/error 
 */
void initAnimation(Animation* anim, 
                   int noOfFrames, 
                   const int SPRITE_WIDTH, const int SPRITE_HEIGHT, 
                   int row, int col)
{

    // set frame count.
    anim->maxFrames = noOfFrames;

    // allocate frame array
    anim->frames = new SDL_Rect[anim->maxFrames]; 

     //Setup animation frames - fixed row!
    for(int i = 0; i < anim->maxFrames; i++)
    {
        if(row == -1)
        {
            anim->frames[i].x = (i * SPRITE_WIDTH); //ith col.
            anim->frames[i].y = (col* SPRITE_HEIGHT); //col row. 
        }
        else 
        {
            if(col == -1)
            {
                anim->frames[i].x = (row * SPRITE_WIDTH); //ith col.
                anim->frames[i].y = (i * SPRITE_HEIGHT); //col row. 
            }
            else
            {
                printf("Bad paramters to initAnimation!\n");
                //return an error?
            }
            
        }
        
        anim->frames[i].w = SPRITE_WIDTH;
        anim->frames[i].h = SPRITE_HEIGHT;
    }

    //set current animation frame to first frame. 
    anim->currentFrame = 0;

    //zero frame time accumulator
    anim->accumulator = 0.0f;
}

/**
 * destAnimation
 * 
 * Function to clean up an animation structure.  
 * 
 * @param anim Animation structure to destroy
 */
void destAnimation(Animation* anim)
{
    //Free the memory - we allocated it with new
    // so must use the matching delete operation.

    delete anim->frames;

    // Good practice to set pointers to null after deleting
    // ths prevents accidental access. 
    anim->frames = nullptr;
}
